Project by Max Myers

This is the base project for RMIT GDS3 - 3D game design and Unity

This project uses Ben Esposito's First Person Drift Controller: http://torahhorse.com/first-person-drifter-controller-for-unity3d

This project is intended as a base for students to build from and includes a set of scripts to help them get started.



The most important of these is the PlayerLook script. In order to use any other of the others this must be attached to the player characters camera object.

Each of the others will add a new interaction. 

ChangeMaterial will allow you to swap the materials of an object on mouse click.

ClickText will display text to the player on mouse click.

ParticlePlay will start and stop an attached particle system.

MakeInvisible will turn on and off a objects MeshRenderer.

Destory will destroy the object it is attached to.


The intention of this is to give students a place to start from and a base project from which they can start learning and playing with unity.