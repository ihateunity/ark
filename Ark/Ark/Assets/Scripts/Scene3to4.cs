﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene3to4 : MonoBehaviour
{
    public float targetTime = 60.0f;
    public GameObject Scene3;
    public GameObject Scene4;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }

    }
    void timerEnded()
    {
        //do your stuff here.
        Scene3.SetActive(false);
        Scene4.SetActive(true);
    }

}
