﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockpitToArk : MonoBehaviour
{
	public float targetTime = 60.0f;
	public GameObject Cockpit;
	public GameObject Ark;
	//public GameObject FadeIn;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		targetTime -= Time.deltaTime;

		if (targetTime <= 0.0f)
		{
			timerEnded();
		}

	}
	void timerEnded()
	{
		//do your stuff here.
		Cockpit.SetActive(false);
		Ark.SetActive(true);
		//FadeIn.SetActive(true);
	}

}
