﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene1to2 : MonoBehaviour
{
    public float targetTime = 60.0f;
    public GameObject Scene1;
    public GameObject Scene2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }

    }
    void timerEnded()
    {
        //do your stuff here.
        Scene1.SetActive(false);
        Scene2.SetActive(true);
    }

}
